#include "EmonLib.h"
#include <SoftwareSerial.h>
#include "Timer.h" //Download lib external http://playground.arduino.cc/Code/Timer
#include <EEPROM.h>
#include <Arduino.h>

EnergyMonitor emon1,emon2,emon3,emon4;
SoftwareSerial esp8266(2,3); 
#define BUFFER_SIZE 64
char buffer[BUFFER_SIZE];
bool _espReady = false;
#define DEBUG true
#define LEDTEST 13
//9 10 11 12 button
//CT A5 A4 A3 A2
// 5 6 7 8 deplay
#define diP1 5
#define diP2 6
#define diP3 7
#define diP4 8

Timer t;
bool _send = false,_ack = true;
double gCt1,gCt2,gCt3,gCt4;

struct __eeprom_data
{
    bool plug1;
    bool plug2;
    bool plug3;
    bool plug4;    
    bool lock1;
    bool lock2;
    bool lock3;
    bool lock4;
    
    char currenttrans[4];
    bool isSetting;
    bool callServer;//1=call server  
    //byte store[202];
} gConfiguration;

#define eeprom_read_to(dst_p, eeprom_field, dst_size) eeprom_read_block(dst_p, (void *)offsetof(__eeprom_data, eeprom_field), MIN(dst_size, sizeof((__eeprom_data*)0)->eeprom_field))
#define eeprom_read(dst, eeprom_field) eeprom_read_to(&dst, eeprom_field, sizeof(dst))
#define eeprom_write_from(src_p, eeprom_field, src_size) eeprom_write_block(src_p, (void *)offsetof(__eeprom_data, eeprom_field), MIN(src_size, sizeof((__eeprom_data*)0)->eeprom_field))
#define eeprom_write(src, eeprom_field) { typeof(src) x = src; eeprom_write_from(&x, eeprom_field, sizeof(x)); }
#define MIN(x,y) ( x > y ? y : x )
void readSetting()
{
        #if DEBUG
        Serial.println(F("Load config"));
        #endif
      
        eeprom_read(gConfiguration.callServer, callServer);       
        eeprom_read(gConfiguration.plug1, plug1);
        eeprom_read(gConfiguration.plug1, plug1);
        eeprom_read(gConfiguration.plug2, plug2);
        eeprom_read(gConfiguration.plug3, plug3);
        eeprom_read(gConfiguration.plug4, plug4);
        
        eeprom_read(gConfiguration.lock1, lock1);
        eeprom_read(gConfiguration.lock2, lock2);
        eeprom_read(gConfiguration.lock3, lock3);
        eeprom_read(gConfiguration.lock4, lock4);                 
        
        eeprom_read(gConfiguration.currenttrans[0], currenttrans[0]);
        eeprom_read(gConfiguration.currenttrans[1], currenttrans[1]);
        eeprom_read(gConfiguration.currenttrans[2], currenttrans[2]);
        eeprom_read(gConfiguration.currenttrans[3], currenttrans[3]);
        #if DEBUG 
         Serial.println(F("--configed"));
        #endif
        
}
void powerControl(bool iplug1,bool iplug2,bool iplug3,bool iplug4);
void powerLock(bool plug1,bool plug2,bool plug3,bool plug4);
void Interval();
void setup() {
     Serial.begin(9600); 
     esp8266.begin(9600); 
// put your setup code here, to run once:
    #if DEBUG
      Serial.println(F("-Setuping"));
    #endif

      pinMode(LEDTEST, OUTPUT);
      pinMode(diP1,OUTPUT);
      pinMode(diP2,OUTPUT);
      pinMode(diP3,OUTPUT);
      pinMode(diP4,OUTPUT);
//For show 
        digitalWrite(diP1,LOW);
        digitalWrite(diP2,LOW);
        digitalWrite(diP3,LOW);
        digitalWrite(diP4,LOW);

          emon1.current(5,0.10);
          emon2.current(4,0.10);
          emon3.current(3,0.10);
          emon4.current(2,0.10);
          
          t.every(10000, Interval);
    #if DEBUG
      Serial.println(F("-Setup"));
    #endif
}
void clearBuffer(void);
String Command(char *cmd);


void loop() {
  // put your main code here, to run repeatedly:
    #if DEBUG   
    clearBuffer();
    #endif
    
    /*if(_espReady)
      delay(500);
    else
      delay(1000);
    */
    delay(500);  
    t.update();
    
   
   if(_ack){
       _ack = false;
      esp8266.println("C");
    }
    
    if(esp8266.available()){
      esp8266.readBytesUntil('\n', buffer, BUFFER_SIZE);
    #if DEBUG
      Serial.println(buffer);
    #endif
       if(strncmp(buffer, "CMD", 3)==0)
       {
           Command(buffer);
       }else if(strncmp(buffer, "RE", 2)==0){
         _espReady = true;
       }else if(strncmp(buffer, "NO", 2)==0){
         _espReady = false;
       }else if(strncmp(buffer, "CONED", 5)==0){
        _espReady = true;
       }else if(strncmp(buffer, "NCONE", 5)==0){
         _espReady = false;
       }
    
     
    }
    if(_send&&_espReady)
   {
    #if DEBUG
      Serial.println("Sent");
    #endif
    _send =false;
    esp8266.println("MSG20,20,50,10S11111100");  
   }
  
}
//Open/Close
void powerControl(bool iplug1,bool iplug2,bool iplug3,bool iplug4)
{
       #if DEBUG
        Serial.print(F("plug1:"));Serial.println(iplug1);
        Serial.print(F("plug2:"));Serial.println(iplug2);
        Serial.print(F("plug3:"));Serial.println(iplug3);
        Serial.print(F("plug4:"));Serial.println(iplug4);
        #endif
        
          digitalWrite(diP1,(iplug1)?HIGH:LOW);
          digitalWrite(diP2,(iplug2)?HIGH:LOW);
          digitalWrite(diP3,(iplug3)?HIGH:LOW);
          digitalWrite(diP4,(iplug4)?HIGH:LOW);
          gConfiguration.plug1 = iplug1;
          gConfiguration.plug2 = iplug2;
          gConfiguration.plug3 = iplug3;
          gConfiguration.plug4 = iplug4;
          eeprom_write(iplug1, plug1);
          eeprom_write(iplug2, plug2);
          eeprom_write(iplug3, plug3);
          eeprom_write(iplug4, plug4);
}

void powerLock(bool plug1,bool plug2,bool plug3,bool plug4)
{
       #if DEBUG
        Serial.print(F("Lplug1:"));Serial.println(plug1);
        Serial.print(F("Lplug2:"));Serial.println(plug2);
        Serial.print(F("Lplug3:"));Serial.println(plug3);
        Serial.print(F("Lplug4:"));Serial.println(plug4);
        #endif
         
}
int timeAck = 0;
void Interval()
{
  if(timeAck ==3){
  _ack = true;
  timeAck = 0;
  }
  timeAck++;
  if(_espReady){
    _send = true;
  }
}
String Command(char *cmd)
{
  #if DEBUG
     Serial.print(F("Command"));
   #endif
   powerControl(*(cmd+3)=='1',*(cmd+4)=='1',*(cmd+5)=='1',*(cmd+6)=='1');
   powerLock(*(cmd+7)=='1',*(cmd+8)=='1',*(cmd+9)=='1',*(cmd+10)=='1');
}
void clearBuffer(void) {
       for (int i =0;i<BUFFER_SIZE;i++ ) {
         buffer[i]=0;
       }
}
