#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include <WiFiUdp.h>
#define DEBUG  true 
WiFiUDP Udp;
#define BUFFER_SIZE 64
#define AIO_SERVER      "m20.cloudmqtt.com"
#define AIO_SERVERPORT  19379
#define AIO_USERNAME    "orkvalaf"
#define AIO_KEY         "B1MFeFCe2pIp"

char buffer[BUFFER_SIZE];
WiFiClient client;
const char MQTT_SERVER[] PROGMEM    = AIO_SERVER;
const char MQTT_CLIENTID[] PROGMEM  = __TIME__ AIO_USERNAME;
const char MQTT_USERNAME[] PROGMEM  = AIO_USERNAME;
const char MQTT_PASSWORD[] PROGMEM  = AIO_KEY;
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, AIO_SERVERPORT, MQTT_CLIENTID, MQTT_USERNAME, MQTT_PASSWORD);
/****************************** Feeds ***************************************/
// Setup a feed called 'photocell' for publishing.
// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>
const char pub[] PROGMEM =  "/pub/msg";
Adafruit_MQTT_Publish pubmsg = Adafruit_MQTT_Publish(&mqtt, pub);
const char cmd[] PROGMEM =  "/sub/cmd";
Adafruit_MQTT_Subscribe subcmd  = Adafruit_MQTT_Subscribe(&mqtt, cmd);;

bool _connectServer = false;
void setup() {
  int cnt = 0;
   #if DEBUG
   Serial.print("Start");
  #endif
  Serial.begin(9600);
    WiFi.mode(WIFI_STA);  
 
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    #if DEBUG
     Serial.print(".");
    #endif
    if(cnt++ >= 10){
       WiFi.beginSmartConfig();
       while(1){
           delay(1000);
           if(WiFi.smartConfigDone()){
             
             break;
           }
       }
    }
  }
  #if DEBUG
    Serial.println("");
    Serial.println("");
    Serial.println("Setup done");
  #endif  
  mqtt.subscribe(&subcmd);
}
void clearBuffer();
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    _connectServer = true;
    return;
  }
  #if DEBUG
  Serial.print("Connecting to MQTT... ");
 #endif
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
      #if DEBUG
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       #endif
       _connectServer = false; 
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
  }
  #if DEBUG
  Serial.println("MQTT Connected!");
  #endif
}

void loop() {
  // Check if a client has connected
  //Case Take form UNO
  MQTT_connect();
  if(Serial.available())
  {      
         #if DEBUG
          clearBuffer() ;
         #endif 
          Serial.readBytesUntil('\n', buffer, BUFFER_SIZE);
          #if DEBUG       
          Serial.println(buffer);
          #endif 
          if(strncmp(buffer, "C", 1)==0)
          {
                //Call back status
                Serial.println((_connectServer)?"RE":"NO"); //R = ready,N = not
          }else if(strncmp(buffer, "MSG", 3)==0){                     
            if (pubmsg.publish(buffer)) {
                  #if DEBUG
                  Serial.println(F("OK!"));
                  #endif
                } else {
                  #if DEBUG
                  Serial.println(F("Failed"));
                  #endif                 
                }
          }
  }
          Adafruit_MQTT_Subscribe *subscription;
          while ((subscription = mqtt.readSubscription(1000))) {
            if (subscription == &subcmd) {
              #if DEBUG
                Serial.println(F("Got: "));
              #endif 
              Serial.println((char *)subcmd.lastread);
            }
          }
}
  

void clearBuffer(void) {
       for (int i =0;i<BUFFER_SIZE;i++ ) {
         buffer[i]=0;
       }
}



  

