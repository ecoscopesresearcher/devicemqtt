#define BUFFER_SIZE 64
char buffer[BUFFER_SIZE];

void setup() {
  Serial.begin(115200);
  Serial.print("Start");   // Initialize the BUILTIN_LED pin as an output
}

// the loop function runs over and over again forever
void loop() {
   Serial.print("loop");   // Turn the LED on (Note that LOW is the voltage level
                                    // but actually the LED is on; this is because 
                                    // it is acive low on the ESP-01)
  delay(1000);
  if(Serial.available())
  {
     Serial.readBytesUntil('\n', buffer, BUFFER_SIZE);
     Serial.println(buffer);
  }
}
